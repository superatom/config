<?php namespace Superatom\Config;

use Illuminate\Filesystem\Filesystem;

class Repository
{
    /** @var array */
    protected $config = [];

    /** @var string */
    protected $configDir;

    /** @var Filesystem */
    protected $fs;

    public function __construct($dir, Filesystem $fs = null)
    {
        $this->configDir = $dir;
        $this->fs = ($fs) ?: new Filesystem();

        $this->loadConfig();
    }

    /**
     * Get config value by dot notation key
     *
     * Config structure example below
     *
     * FILE: configDir/app.php
     * <?php
     * return [
     *      'debug' => true,                // app.debug
     *      'timezone' => 'Asia/Tokyo',     // app.timezone
     *      'log' => [                      // app.log
     *          'output' => '/path/to/log', // app.log.output
     *          'severity' => 'info',       // app.log.severity
     *      ],
     * ];
     *
     * @param string $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return array_get($this->config, $key, $default);
    }

    /**
     * Determine if given config name exists
     *
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return array_has($this->config, $key);
    }

    /**
     * Get all config values
     *
     * @return array
     */
    public function all()
    {
        return $this->config;
    }

    /**
     * Load all config file from local filesystem
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function loadConfig()
    {
        $files = $this->fs->glob($this->configDir . '/*.php');

        foreach ($files as $file) {
            $group = basename($file, '.php');
            $this->config[$group] = $this->fs->getRequire($file);
        }
    }
}

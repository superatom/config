<?php

if ( ! function_exists('env'))
{
    /**
     * Get environment variable value if exists
     * Support boolean values
     *
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $val = getenv($key);

        if ($val === false) {
            return $default;
        }

        if (strtolower($val) === 'true') {
            $val = true;
        }

        if (strtolower($val) === 'false') {
            $val = false;
        }

        return $val;
    }
}

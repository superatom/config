# Superatom Config

A simple ReadOnly configuration library

## Install

Require this package in your composer.json

    "superatom/config": "dev-master"

## Usage

example configuration file store to `path/to/config/app.php`  
`path/to/config` dir contains only config files

```php
<?php // app.php

return [
    'debug' => true,
    'hoge' => 'fuga',
    'array' => [
        'key1' => 'val1',
        'key2' => 'val2',
    ],
    'environment' => env('ENV_NAME', 'default_value'),
    'bool_env' => env('DEBUG_MODE', false),
];
```

use app.php config example

```php
<?php
require_once 'path/to/vendor/autoload.php';

putenv('DEBUG_MODE=true');

$config = new \Superatom\Config\Repository('path/to/config');

// get config value by dot notation key

var_dump($config->get('app.debug'));
// bool(true)

var_dump($config->get('app.array.key1'));
// string(4) "val1"

// environment variables value is always string.
// but 'true' or 'false' string convert to PHP boolean value automatically.
var_dump($config->get('app.bool_env'));
// bool(true)

var_dump($config->get('app'));
/*
array(5) {
  'debug' =>
  bool(true)
  'hoge' =>
  string(4) "fuga"
  'array' =>
  array(2) {
    'key1' =>
    string(4) "val1"
    'key2' =>
    string(4) "val2"
  }
  'environment' =>
  string(13) "default_value"
  'bool_env' =>
  bool(true)
}
*/
```

#### helper function

`env($name, $default = null)`

return environment value if exists (supports boolean value)

### License

MIT
